COMPLETED GAME IS IN LIGHTING BRANCH, NOT DEVELOP OR MASTER BRANCH.
Please checkout 'Lighting' branch.

Team: Roger Li 585361, William Cao 696120, Dean Peach 699481

Video link: https://www.youtube.com/channel/UCIA06juL2q0v1kU54ezP9dg

**1. What the application does**
The game is a 3-dimensional version of the arcade game Breakout by Atari. 

**2. How to use it (especially the user interface aspects)**
The player will control a paddle to continuously bounce a ball back into the bricks. The player will be able to control the horizontal and vertical movement of the paddle. It will also be possible to move the paddle forwards. The player can acquire power-ups by hitting certain blocks. 

We envision that the goal of the game will be to break enough of the bricks such that the paddle is able to move through to the end of the tunnel. There are two life systems in the game: number of balls and paddle hitpoints; the player has a limited amount of balls and a limited number of collisions allowed between the paddle and the bricks.

The game is set in a dark tunnel. The player can hit certain blocks to light up the tunnel momentarily to help them traverse the tunnel.

Paddle movement is hopefully rather intuitive, utilising a joystick and a button for advancing. Shaking the device will commence the ball movement (this is detected as a change in the z component of the acceleration as detected by the accelerometer of the device), as well as “kicking” the ball (applying a forward force) in the situation where the ball is not moving backwards or forwards fast enough.

**3. How you modelled objects and entities**
All the objects in the game are instantiated at the start of the game. The paddle is created at the centre of the camera space. The ball, paddle and bricks all have rigidbodies which give them physic properties. The ball has no friction, which means that it doesn't slow down after hitting the paddle or bricks. 

**4. How you handled graphics and camera motion, and**
The locations of the bricks are randomised. Most bricks use a Blinn-Phong shader, modified so that they are responsive to changes in point light intensity. Other bricks use a shader which will create a glow effect. There are also bricks which use a number of particle systems.
The ball has a trailing particle system, while the paddle is partially transparent.
The tunnel wall uses a texture and a normal 

The camera motion is tied to the motion of the paddle; when the paddle advances, so does the camera. When the paddle is blocked from moving because of bricks in front of it, the camera is also unable to move. 

**5. A statement about any code/APIs you have sourced/used from the
internet that is not your own**
The pink sparkly orb was created using textures from here: http://vuducfreetextures.gr8.com/
Its modelling is also mostly inspired by this video: https://www.youtube.com/watch?v=ppYZsPHI234
Some assets for the cave wall were found here: http://polycount.com/discussion/125857/
Code from the workshops was refactored for some parts of the project, in particular the custom shader.